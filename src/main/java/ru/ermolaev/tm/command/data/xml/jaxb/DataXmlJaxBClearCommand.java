package ru.ermolaev.tm.command.data.xml.jaxb;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataXmlJaxBClearCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-xml-jb-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove XML (Jax-B) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE XML (JAX-B) FILE]");
        final File file = new File(DataConstant.FILE_XML_FX);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
