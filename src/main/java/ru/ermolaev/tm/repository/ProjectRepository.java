package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;
import ru.ermolaev.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public void clear(final String userId) {
        projects.removeAll(findAll(userId));
    }

    @Override
    public Project findById(final String userId, final String id) throws Exception {
        for (final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        throw new UnknownIdException(id);
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) throws Exception {
        for (final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (projects.indexOf(project) == index) return project;
        }
        throw new UnknownIndexException(index);
    }

    @Override
    public Project findByName(final String userId, final String name) throws Exception {
        for (final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        throw new UnknownNameException(name);
    }

    @Override
    public Project removeById(final String userId, final String id) throws Exception {
        final Project project = findById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) throws Exception {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) throws Exception {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        projects.add(project);
        return project;
    }

    @Override
    public void add(final List<Project> projects) {
        for (final Project project: projects) add(project);
    }

    @Override
    public void add(final Project... projects) {
        for (final Project project: projects) add(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public void load(final List<Project> projects) {
        clear();
        add(projects);
    }

    @Override
    public void load(final Project... projects) {
        clear();
        add(projects);
    }

    @Override
    public List<Project> getProjectsList() {
        return projects;
    }

}
