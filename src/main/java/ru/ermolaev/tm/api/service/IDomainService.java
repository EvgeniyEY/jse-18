package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
