package ru.ermolaev.tm.exception.unknown;

import ru.ermolaev.tm.exception.AbstractException;

public class UnknownEmailException extends AbstractException {

    public UnknownEmailException() {
        super("Error! This email does not exist.");
    }

    public UnknownEmailException(final String email) {
        super("Error! This email [" + email + "] does not exist.");
    }

}
