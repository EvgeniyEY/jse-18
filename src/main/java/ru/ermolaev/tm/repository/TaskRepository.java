package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;
import ru.ermolaev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public void clear(final String userId) {
        tasks.removeAll(findAll(userId));
    }

    @Override
    public Task findById(final String userId, final String id) throws Exception {
        for (final Task task: tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        throw new UnknownIdException(id);
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) throws Exception {
        for (final Task task: tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (tasks.indexOf(task) == index) return task;
        }
        throw new UnknownIndexException(index);
    }

    @Override
    public Task findByName(final String userId, final String name) throws Exception {
        for (final Task task: tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        throw new UnknownNameException(name);
    }

    @Override
    public Task removeById(final String userId, final String id) throws Exception {
        final Task task = findById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) throws Exception {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) throws Exception {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        tasks.add(task);
        return task;
    }

    @Override
    public void add(final List<Task> tasks) {
        for (final Task task: tasks) add(task);
    }

    @Override
    public void add(final Task... tasks) {
        for (final Task task: tasks) add(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void load(final List<Task> tasks) {
        clear();
        add(tasks);
    }

    @Override
    public void load(final Task... tasks) {
        clear();
        add(tasks);
    }

    @Override
    public List<Task> getTasksList() {
        return tasks;
    }

}
