package ru.ermolaev.tm.exception.user;

import ru.ermolaev.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }

}
