package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.service.IDomainService;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    public DomainService(
            final ITaskService taskService,
            final IProjectService projectService,
            final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setTasks(taskService.getTasksList());
        domain.setProjects(projectService.getProjectsList());
        domain.setUsers(userService.findAll());
    }

}
