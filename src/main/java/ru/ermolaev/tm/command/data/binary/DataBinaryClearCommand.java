package ru.ermolaev.tm.command.data.binary;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBinaryClearCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-bin-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE BINARY FILE]");
        final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
