package ru.ermolaev.tm.command.data.json.fasterxml;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataJsonFXmlClearCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-json-fx-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove json (fasterXML) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE JSON (FASTERXML) FILE]");
        final File file = new File(DataConstant.FILE_JSON_FX);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
