package ru.ermolaev.tm.command.data.xml.fasterxml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlFXmlLoadCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-xml-fx-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from XML (fasterXML) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (FASTERXML) LOAD]");
        final String xmlData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML_FX)));
        final XmlMapper xmlMapper = new XmlMapper();
        final Domain domain = xmlMapper.readValue(xmlData, Domain.class);

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
