package ru.ermolaev.tm.command.data.base64;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;

public class DataBase64SaveCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-base64-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        final String base64 = new BASE64Encoder().encode(bytes);
        byteArrayOutputStream.close();

        final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
