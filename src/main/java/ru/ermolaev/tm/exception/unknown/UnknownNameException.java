package ru.ermolaev.tm.exception.unknown;

import ru.ermolaev.tm.exception.AbstractException;

public class UnknownNameException extends AbstractException {

    public UnknownNameException() {
        super("Error! This name does not exist.");
    }

    public UnknownNameException(final String name) {
        super("Error! This name [" + name + "] does not exist.");
    }

}
