package ru.ermolaev.tm.command.data.json.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonFXmlLoadCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-json-fx-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from json (fasterXML) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FASTERXML) LOAD]");
        final String jsonData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON_FX)));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = objectMapper.readValue(jsonData, Domain.class);

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
