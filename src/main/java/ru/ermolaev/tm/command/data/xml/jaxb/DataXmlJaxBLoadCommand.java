package ru.ermolaev.tm.command.data.xml.jaxb;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlJaxBLoadCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-xml-jb-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from XML (Jax-B) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) LOAD]");

        File xmlFile = new File(DataConstant.FILE_XML_JB);
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(xmlFile);

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
