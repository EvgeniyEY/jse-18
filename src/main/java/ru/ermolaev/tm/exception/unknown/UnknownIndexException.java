package ru.ermolaev.tm.exception.unknown;

import ru.ermolaev.tm.exception.AbstractException;

public class UnknownIndexException extends AbstractException {

    public UnknownIndexException() {
        super("Error! This index does not exist.");
    }

    public UnknownIndexException(final Integer index) {
        super("Error! This index [" + index + "] does not exist.");
    }

}
