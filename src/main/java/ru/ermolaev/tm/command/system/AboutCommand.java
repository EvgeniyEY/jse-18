package ru.ermolaev.tm.command.system;

import ru.ermolaev.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Evgeniy Ermolaev");
        System.out.println("E-MAIL: ermolaev.evgeniy.96@yandex.ru");
    }

}
